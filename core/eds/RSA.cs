﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace core.utils
{
    public class RSA
    {
        private static CngKey k;
        private static RSACng rsa_cng;

        public static bool ExistContainer(string name)
        {
            return CngKey.Exists(name);
        }

        public static void CreateContainer(string name)
        {
            if (ExistContainer(name)) throw new InvalidOperationException("this container exist");
            k = CngKey.Create(CngAlgorithm.Rsa, name);
            rsa_cng = new RSACng(k);
        }

        public static void SetContainer(string name)
        {
            if (!ExistContainer(name)) throw new InvalidOperationException("this container not exist");
            k = CngKey.Open(name);
            rsa_cng = new RSACng(k);
        }

        public static void DeleteContainer(string name)
        {
            if (!ExistContainer(name)) return;
            k.Delete();
        }

        public static void ImportPublicKey(byte[] data)
        {
            k = CngKey.Import(data, CngKeyBlobFormat.GenericPublicBlob);
            rsa_cng = new RSACng(k);
        }

        public static byte[] ExportPublicKey(string name)
        {
            if (!ExistContainer(name)) throw new InvalidOperationException("this container not exist");
            return k.Export(CngKeyBlobFormat.GenericPublicBlob);
        }
        static public byte[] GetEDS(byte[] data)
        {
            return rsa_cng.SignData(data, HashAlgorithmName.SHA256, RSASignaturePadding.Pkcs1);
        }

        static public bool CheckEDS(byte[] data, byte[] signature)
        {
            return rsa_cng.VerifyData(data, signature, HashAlgorithmName.SHA256, RSASignaturePadding.Pkcs1);
        }
    }
}
