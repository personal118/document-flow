﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace core.utils
{
    public class ECDSA
    {
        private static CngKey k;
        private static ECDsaCng ecdsa_cng;

        public static bool ExistContainer(string name)
        {
            return CngKey.Exists(name);
        }

        public static void CreateContainer(string name)
        {
            if (ExistContainer(name)) throw new InvalidOperationException("this container exist");
            k = CngKey.Create(CngAlgorithm.ECDsaP256, name);
            ecdsa_cng = new ECDsaCng(k);
        }

        public static void SetContainer(string name)
        {
            if (!ExistContainer(name)) throw new InvalidOperationException("this container not exist");
            k = CngKey.Open(name);
            ecdsa_cng = new ECDsaCng(k);
        }

        public static void DeleteContainer(string name)
        {
            if (!ExistContainer(name)) return;
            k.Delete();
        }

        public static void ImportPublicKey(byte[] data)
        {
            k = CngKey.Import(data, CngKeyBlobFormat.EccPublicBlob);
            ecdsa_cng = new ECDsaCng(k);
        }

        public static byte[] ExportPublicKey(string name)
        {
            if (!ExistContainer(name)) throw new InvalidOperationException("this container not exist");
            return k.Export(CngKeyBlobFormat.EccPublicBlob);
        }

        static public byte[] GetEDS(byte[] data)
        {
            ecdsa_cng.HashAlgorithm = CngAlgorithm.Sha512;
            return ecdsa_cng.SignData(data);
        }

        static public bool CheckEDS(byte[] data, byte[] signature)
        {
            ecdsa_cng.HashAlgorithm = CngAlgorithm.Sha512;
            return ecdsa_cng.VerifyData(data, signature);
        }
    }
}
