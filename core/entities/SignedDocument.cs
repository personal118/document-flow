﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace core.entities
{
    public class SignedDocument
    {
        public string UserName;
        public byte[] edsblob;
        public string text;

        public SignedDocument()
        {
            UserName = "";
            edsblob = new byte[] { };
            text = "";
        }

        public SignedDocument(string fname)
        {
            SignedDocument d;
            if (!LoadFromFile(fname, out d))
                throw new Exception();
            UserName = d.UserName;
            edsblob = d.edsblob;
            text = d.text;
        }
        
        private static bool LoadFromFile(string fname, out SignedDocument doc)
        {
            try
            {
                doc = new SignedDocument();
                using (BinaryReader sr = new BinaryReader(new FileStream(fname, FileMode.Open, FileAccess.Read)))
                {
                    int namelen, edslen;
                    namelen = sr.ReadInt32();
                    edslen = sr.ReadInt32();
                    string uname;
                    uname = Encoding.ASCII.GetString(sr.ReadBytes(namelen));
                    byte[] eb = sr.ReadBytes(edslen);
                    string sts;
                    using (StreamReader ssr = new StreamReader(sr.BaseStream, Encoding.Unicode))
                    {
                        sts = ssr.ReadToEnd();
                    }
                    doc.edsblob = eb;
                    doc.text = sts;
                    doc.UserName = uname;
                }
                return true;
            }
            catch
            {
                doc = null;
                return false;
            }
        }

        public void Save(string fname)
        {
            using (BinaryWriter bw = new BinaryWriter(new FileStream(fname, FileMode.Create, FileAccess.Write)))
            {
                bw.Write(UserName.Length);
                bw.Write(edsblob.Length);
                bw.Write(Encoding.ASCII.GetBytes(UserName));
                bw.Write(edsblob);
                bw.Write(Encoding.Unicode.GetBytes(text));
            }
        }
    }
}
