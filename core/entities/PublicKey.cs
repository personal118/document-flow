﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace core.entities
{
    public class PublicKey
    {
 
        public string Owner;
        public byte[] Blob;
        public byte[] Eds;
              
        public PublicKey()
        {
            Owner = "";
            Blob = new byte[] { };
            Eds = null;
        }

        public PublicKey(string fname)
        {
            using (BinaryReader br = new BinaryReader(new FileStream(fname, FileMode.Open, FileAccess.Read)))
            {
                int namlen = br.ReadInt32();
                int pklen = br.ReadInt32();
                Owner = Encoding.UTF8.GetString(br.ReadBytes(namlen));
                Blob = br.ReadBytes(pklen);
                if (br.BaseStream.Position < br.BaseStream.Length)
                    Eds = br.ReadBytes((int)(br.BaseStream.Length - br.BaseStream.Position));
                else Eds = null;
            }
        }

        public void Save(string fname)
        {
            using (BinaryWriter bw = new BinaryWriter(new FileStream(fname, FileMode.Create, FileAccess.Write)))
            {
                bw.Write(Owner.Length);
                bw.Write(Blob.Length);
                bw.Write(Encoding.UTF8.GetBytes(Owner));
                bw.Write(Blob);
                if (Eds != null)
                    bw.Write(Eds);
            }
        }

    }
}
