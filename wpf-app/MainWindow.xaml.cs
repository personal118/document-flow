﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using core.utils;
using core.entities;
using Microsoft.Win32;
using System.IO;

namespace wpf_app
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        About a;

        public MainWindow()
        {
            InitializeComponent();
            Directory.CreateDirectory(".\\PK\\");
        }
        
        private void OpenAbout_Click(object sender, RoutedEventArgs e)
        {
            a = new About();
            a.ShowDialog();
        }

        private void Button_SelectPrivate_Click(object sender, RoutedEventArgs e)
        {
            NewUser();
        }

        private void NewUser()
        {
            ActiveContiner(false);
            username.IsEnabled = true;
            username.Text = "";
            username.Focus();
            this.Title = "Document Flow";
        }

        private void Username_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox textBox = sender as TextBox;
            if (textBox != null)
            {
                string theText = textBox.Text;
                if (theText.Length > 0)
                {
                    Button_ConfirmSelectPrivate.IsEnabled = true;
                }
                else
                {
                    Button_ConfirmSelectPrivate.IsEnabled = false;
                }
            }
        }

        private void Button_ConfirmSelectPrivate_Click(object sender, RoutedEventArgs e)
        {
            username.IsEnabled = false;
            Button_ConfirmSelectPrivate.IsEnabled = false;
            
            if (!RSA.ExistContainer(username.Text + "_RSA"))
            {
                if (MessageBox.Show(this, "Пара ключей пользователя с таким именем не найдена. Создать новую пару?", "Ключи не найдены", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    RSA.CreateContainer(username.Text+"_RSA");
                    ECDSA.CreateContainer(username.Text + "_ECDSA");
                }
                else
                {
                    NewUser();
                    return;
                }
            }

            RSA.SetContainer(username.Text + "_RSA");
            ECDSA.SetContainer(username.Text+ "_ECDSA");

            this.Title = username.Text + " | Новый документ ";
            ActiveContiner(true);
        }

        private void ActiveContiner(bool flag)
        {
            Button_Load.IsEnabled = flag;
            Button_Save.IsEnabled = flag;
            Button_SelectPrivate.IsEnabled = flag;
            Menu_ChangePrivate.IsEnabled = flag;
            Menu_Create.IsEnabled = flag;
            Menu_Export.IsEnabled = flag;
            Menu_Import.IsEnabled = flag;
            Menu_RemoveKeys.IsEnabled = flag;
            Menu_Save.IsEnabled = flag;
            Menu_Load.IsEnabled = flag;
            document_text.IsEnabled = flag;
            document_text.Text = "";
            if (flag)
            {
                document_text.Focus();
            }
        }

        private void Menu_Create_Click(object sender, RoutedEventArgs e)
        {
            this.Title = username.Text + " | Новый документ ";
            document_text.Text = "";
            document_text.Focus();
        }

        private void Menu_Load_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog o = new OpenFileDialog();

            o.Filter = "Singed Document (.sd)|*.sd";
            if (o.ShowDialog() == true)
            {
                SignedDocument sd = new SignedDocument(o.FileName);//read document, author name;
                

                PublicKey p;
                try
                {
                    p = new PublicKey(".\\PK\\" + username.Text + "\\" + sd.UserName + ".pk");
                }
                catch (Exception)
                {
                    MessageBox.Show(this, "Открытый ключ Автора не найден.");
                    return;
                }
                

                if (!ECDSA.CheckEDS( p.Blob, p.Eds))
                {
                    MessageBox.Show(this, "Электронная подпись открытого ключа автора документа не подтверждена.");
                    return;
                }

                RSA.ImportPublicKey(p.Blob);

                if (!RSA.CheckEDS(Encoding.Default.GetBytes(sd.text), sd.edsblob))
                {
                    MessageBox.Show(this, "Электронная подпись документа не подтверждена.");
                    return;
                }

                document_text.Text = sd.text;
                document_text.Focus();
                this.Title = this.username.Text + " | " + o.SafeFileName + " | Подписан (" + sd.UserName + ") ";
            }
        }

        private void Menu_Save_Click(object sender, RoutedEventArgs e)
        {
            RSA.SetContainer(username.Text + "_RSA");
            ECDSA.SetContainer(username.Text + "_ECDSA");

            SaveFileDialog s = new SaveFileDialog();
            s.Filter = "Singed Document (.sd)|*.sd";
            if (s.ShowDialog() == true)
            {
                SignedDocument sd = new SignedDocument();
                sd.text = document_text.Text;
                sd.UserName = username.Text;
                sd.edsblob = RSA.GetEDS(Encoding.Default.GetBytes(sd.text));
                sd.Save(s.FileName);
                this.Title = this.username.Text + " | " + s.SafeFileName + " | Подписан (" + sd.UserName + ") ";
            }
        }

        private void Menu_Exit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void Menu_Export_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog s = new SaveFileDialog();
            s.Filter = "Public Key (.pk)|*.pk";
            if (s.ShowDialog() == true)
            {
                PublicKey p = new PublicKey();
                p.Blob = RSA.ExportPublicKey(username.Text + "_RSA");
                p.Owner = username.Text;
                p.Eds = null;
                p.Save(s.FileName);
            }
           
        }

        private void Menu_Import_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog o = new OpenFileDialog();
            o.Multiselect = true;
            o.Filter = "Public Key (.pk)|*.pk";
            if (o.ShowDialog() == true) {

                Directory.CreateDirectory(".\\PK\\" + username.Text + "\\");
                foreach(string file in o.FileNames)
                {
                    PublicKey p = new PublicKey(file);
                    p.Eds = ECDSA.GetEDS(p.Blob);
                    p.Save(".\\PK\\" + username.Text + "\\" + p.Owner + ".pk");
                }
                
            }    
        }

        private void Menu_RemoveKeys_Click(object sender, RoutedEventArgs e)
        {
            RSA.DeleteContainer(username.Text + "_RSA");
            ECDSA.DeleteContainer(username.Text + "_ECDSA");
            Button_SelectPrivate_Click(sender, e);
        }

        private void Menu_ChangePrivate_Click(object sender, RoutedEventArgs e)
        {
            Button_SelectPrivate_Click(sender, e);
        }

        private void Username_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Button_ConfirmSelectPrivate_Click(sender, e);
            }
        }
    }
}
